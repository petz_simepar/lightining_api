/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simepar.rest_raios.model;

import java.time.OffsetDateTime;

/**
 *
 * @author santana
 */
public class Gld {
    private int gldversao;
    private OffsetDateTime glddatahora; 
    private int gldnanosegundo;
    private int gldlatitude;
    private int gldlongitude;
    private double gldmultiplicidade;
    private double gldpicocorrente;
    private int gldnumsensor;
    private int gldgrausliberdade;
    private double gldelipseangulo;
    private double gldelipseeixomaior;    
    private double gldquiquadrado;
    private double gldrisetime;   
    private double gldtempopico;
    private double gldmaxraterise;    
    private double gldindicadornuvem;
    private double gldindicadorangulo;    
    private double gldindicadorsinal;
    private double gldindicadortempo;    
    private double gldelipseeixomenor;
    private String the_centrogeom; 
    private String the_elipsegeom; 
    
    

    
    public int getGldversao() {
        return gldversao;
    }

    public void setGldversao(int gldversao) {
        this.gldversao = gldversao;
    }

    public OffsetDateTime getGlddatahora() {
        return glddatahora;
    }

    public void setGlddatahora(OffsetDateTime glddatahora) {
        this.glddatahora = glddatahora;
    }

    public int getGldnanosegundo() {
        return gldnanosegundo;
    }

    public void setGldnanosegundo(int gldnanosegundo) {
        this.gldnanosegundo = gldnanosegundo;
    }

    public int getGldlatitude() {
        return gldlatitude;
    }

    public void setGldlatitude(int gldlatitude) {
        this.gldlatitude = gldlatitude;
    }

    public int getGldlongitude() {
        return gldlongitude;
    }

    public void setGldlongitude(int gldlongitude) {
        this.gldlongitude = gldlongitude;
    }

    public double getGldmultiplicidade() {
        return gldmultiplicidade;
    }

    public void setGldmultiplicidade(double gldmultiplicidade) {
        this.gldmultiplicidade = gldmultiplicidade;
    }

    public double getGldpicocorrente() {
        return gldpicocorrente;
    }

    public void setGldpicocorrente(double gldpicocorrente) {
        this.gldpicocorrente = gldpicocorrente;
    }

    public int getGldnumsensor() {
        return gldnumsensor;
    }

    public void setGldnumsensor(int gldnumsensor) {
        this.gldnumsensor = gldnumsensor;
    }

    public int getGldgrausliberdade() {
        return gldgrausliberdade;
    }

    public void setGldgrausliberdade(int gldgrausliberdade) {
        this.gldgrausliberdade = gldgrausliberdade;
    }

    public double getGldelipseangulo() {
        return gldelipseangulo;
    }

    public void setGldelipseangulo(double gldelipseangulo) {
        this.gldelipseangulo = gldelipseangulo;
    }

    public double getGldelipseeixomaior() {
        return gldelipseeixomaior;
    }

    public void setGldelipseeixomaior(double gldelipseeixomaior) {
        this.gldelipseeixomaior = gldelipseeixomaior;
    }

    public double getGldquiquadrado() {
        return gldquiquadrado;
    }

    public void setGldquiquadrado(double gldquiquadrado) {
        this.gldquiquadrado = gldquiquadrado;
    }

    public double getGldrisetime() {
        return gldrisetime;
    }

    public void setGldrisetime(double gldrisetime) {
        this.gldrisetime = gldrisetime;
    }

    public double getGldtempopico() {
        return gldtempopico;
    }

    public void setGldtempopico(double gldtempopico) {
        this.gldtempopico = gldtempopico;
    }

    public double getGldmaxraterise() {
        return gldmaxraterise;
    }

    public void setGldmaxraterise(double gldmaxraterise) {
        this.gldmaxraterise = gldmaxraterise;
    }

    public double getGldindicadornuvem() {
        return gldindicadornuvem;
    }

    public void setGldindicadornuvem(double gldindicadornuvem) {
        this.gldindicadornuvem = gldindicadornuvem;
    }

    public double getGldindicadorangulo() {
        return gldindicadorangulo;
    }

    public void setGldindicadorangulo(double gldindicadorangulo) {
        this.gldindicadorangulo = gldindicadorangulo;
    }

    public double getGldindicadorsinal() {
        return gldindicadorsinal;
    }

    public void setGldindicadorsinal(double gldindicadorsinal) {
        this.gldindicadorsinal = gldindicadorsinal;
    }

    public double getGldindicadortempo() {
        return gldindicadortempo;
    }

    public void setGldindicadortempo(double gldindicadortempo) {
        this.gldindicadortempo = gldindicadortempo;
    }

    public double getGldelipseeixomenor() {
        return gldelipseeixomenor;
    }

    public void setGldelipseeixomenor(double gldelipseeixomenor) {
        this.gldelipseeixomenor = gldelipseeixomenor;
    }

    public String getThe_centrogeom() {
        return the_centrogeom;
    }

    public void setThe_centrogeom(String the_centrogeom) {
        this.the_centrogeom = the_centrogeom;
    }

    public String getThe_elipsegeom() {
        return the_elipsegeom;
    }

    public void setThe_elipsegeom(String the_elipsegeom) {
        this.the_elipsegeom = the_elipsegeom;
    }
}
