/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simepar.rest_raios.model;

import java.time.OffsetDateTime;

/**
 *
 * @author santana
 */
public class Rindat {
    private int raiversao;
    private OffsetDateTime raidatahora;
    private int rainanosegundo;
    private double railatitude;
    private double railongitude;
    private double raimultiplicidade;
    private double raipicocorrente;
    private int rainumsensor;
    private int raigrausliberdade;
    private double raielipseangulo;
    private double raielipseeixomaior;
    private double raiquiquadrado;
    private double rairisetime;
    private double raitempopico;
    private double raimaxraterise;
    private double raiindicadornuvem;
    private double raiindicadorangulo;
    private double raiindicadorsinal;
    private double raiindicadortempo;
    private String the_centrogeom;
    private String the_elipsegeom;
    private double raielipseeixomenor;
    
    
//    Para definir de qual tabela vai pegar o dado -------
    private String fontId;

    public String getFontId() {
        return fontId;
    }

    public void setFontId(String fontId) {
        this.fontId = fontId;
    }
    
//   ------------------------------------------------------

    public int getRaiversao() {
        return raiversao;
    }

    public void setRaiversao(int raiversao) {
        this.raiversao = raiversao;
    }

    public OffsetDateTime getRaidatahora() {
        return raidatahora;
    }

    public void setRaidatahora(OffsetDateTime raidatahora) {
        this.raidatahora = raidatahora;
    }

    public int getRainanosegundo() {
        return rainanosegundo;
    }

    public void setRainanosegundo(int rainanosegundo) {
        this.rainanosegundo = rainanosegundo;
    }

    public double getRailatitude() {
        return railatitude;
    }

    public void setRailatitude(double railatitude) {
        this.railatitude = railatitude;
    }

    public double getRailongitude() {
        return railongitude;
    }

    public void setRailongitude(double railongitude) {
        this.railongitude = railongitude;
    }

    public double getRaimultiplicidade() {
        return raimultiplicidade;
    }

    public void setRaimultiplicidade(double raimultiplicidade) {
        this.raimultiplicidade = raimultiplicidade;
    }

    public double getRaipicocorrente() {
        return raipicocorrente;
    }

    public void setRaipicocorrente(double raipicocorrente) {
        this.raipicocorrente = raipicocorrente;
    }

    public int getRainumsensor() {
        return rainumsensor;
    }

    public void setRainumsensor(int rainumsensor) {
        this.rainumsensor = rainumsensor;
    }

    public int getRaigrausliberdade() {
        return raigrausliberdade;
    }

    public void setRaigrausliberdade(int raigrausliberdade) {
        this.raigrausliberdade = raigrausliberdade;
    }

    public double getRaielipseangulo() {
        return raielipseangulo;
    }

    public void setRaielipseangulo(double raielipseangulo) {
        this.raielipseangulo = raielipseangulo;
    }

    public double getRaielipseeixomaior() {
        return raielipseeixomaior;
    }

    public void setRaielipseeixomaior(double raielipseeixomaior) {
        this.raielipseeixomaior = raielipseeixomaior;
    }

    public double getRaiquiquadrado() {
        return raiquiquadrado;
    }

    public void setRaiquiquadrado(double raiquiquadrado) {
        this.raiquiquadrado = raiquiquadrado;
    }

    public double getRairisetime() {
        return rairisetime;
    }

    public void setRairisetime(double rairisetime) {
        this.rairisetime = rairisetime;
    }

    public double getRaitempopico() {
        return raitempopico;
    }

    public void setRaitempopico(double raitempopico) {
        this.raitempopico = raitempopico;
    }

    public double getRaimaxraterise() {
        return raimaxraterise;
    }

    public void setRaimaxraterise(double raimaxraterise) {
        this.raimaxraterise = raimaxraterise;
    }

    public double getRaiindicadornuvem() {
        return raiindicadornuvem;
    }

    public void setRaiindicadornuvem(double raiindicadornuvem) {
        this.raiindicadornuvem = raiindicadornuvem;
    }

    public double getRaiindicadorangulo() {
        return raiindicadorangulo;
    }

    public void setRaiindicadorangulo(double raiindicadorangulo) {
        this.raiindicadorangulo = raiindicadorangulo;
    }

    public double getRaiindicadorsinal() {
        return raiindicadorsinal;
    }

    public void setRaiindicadorsinal(double raiindicadorsinal) {
        this.raiindicadorsinal = raiindicadorsinal;
    }

    public double getRaiindicadortempo() {
        return raiindicadortempo;
    }

    public void setRaiindicadortempo(double raiindicadortempo) {
        this.raiindicadortempo = raiindicadortempo;
    }

    public String getThe_centrogeom() {
        return the_centrogeom;
    }

    public void setThe_centrogeom(String the_centrogeom) {
        this.the_centrogeom = the_centrogeom;
    }

    public String getThe_elipsegeom() {
        return the_elipsegeom;
    }

    public void setThe_elipsegeom(String the_elipsegeom) {
        this.the_elipsegeom = the_elipsegeom;
    }

    public double getRaielipseeixomenor() {
        return raielipseeixomenor;
    }

    public void setRaielipseeixomenor(double raielipseeixomenor) {
        this.raielipseeixomenor = raielipseeixomenor;
    }
    
}
